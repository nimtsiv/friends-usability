const { Router } = require("express");
const User = require("../models/User");
const { ensureToken, withAuth } = require("../middleware/auth.middleware");
const router = Router();

router.use(withAuth); // midlware for CORS
router.use(ensureToken); // midlware for getting User Data from basic

// /api/user/   get user data
router.get("/", async (req, res) => {
 
    res.json(req.user);
  
});



module.exports = router;
