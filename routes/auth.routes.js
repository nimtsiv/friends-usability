const {Router} = require('express');
const bodyParser = require('body-parser');
const {check, validationResult} = require('express-validator');
const User = require('../models/User');
const {withoutAuth} = require("../middleware/auth.middleware");
const config = require('config');
const bcrypt = require('bcryptjs');
const router = Router();

router.use(withoutAuth);
router.use(bodyParser.json());


// /api/auth/login
router.post('/login',
    [
        check('email', 'Введіть коректний емайл').isEmail(),
        check('password', 'Введіть пароль').exists()
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    error: errors.array(),
                    message: ' Не коректні дані для входу '
                });
            }

            const {email, password} = req.body;
            const user = await User.findOne({email});


            if (!user) {

                return res.status(401).json({message: 'Користувача не знайдено'})
            }


            const match = await bcrypt.compare(password, user.password);

            if (!match) {
                return res.status(401).json({message: 'Невірно введений пароль'})
            }

            let baseString = new Buffer.from(email + ':' + password).toString('base64');

            res.set('Authorization', 'Basic ' + baseString);
            res.sendStatus(200);

        } catch
            (e) {
            console.log(e);
            res.status(500).json({message: 'Щось пішло не так, попробуйте знову пізніше'});
        }

    });


module.exports = router;