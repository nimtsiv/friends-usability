const { Router } = require("express");
const User = require("../models/User");
const { ensureToken, withAuth } = require("../middleware/auth.middleware");
const router = Router();

router.use(withAuth); // midlware for CORS
router.use(require('body-parser').json())
router.use(ensureToken); // midlware for getting User Data from basic

// /api/friend/   find friend by val = userSearchParam
router.get("/search/:userSearchParam", async (req, res) => {
  let user = await User.find(
    { name: { $regex: req.params["userSearchParam"], $options: "i" } },
    { name: 1, _id: 1 }
  );

 
  if (user.length > 0) {
    const newuser = user.filter(
      (user) => req.user._id.toString() !== user._id.toString()
    );
    res.json(newuser);
  } else {
    res.sendStatus(204);
  }
});

//api/friend/addFriend/    -   Sent add friend request
router.put("/addFriend/:friendId", async (req, res) => {
  // assign request to user which requesting
    //console.log(req.body)
  const friendId = req.params["friendId"];
  let pendingRequestFrom = {
    friendId: friendId,
    isFrom: false,
    name: req.body.name,
  };

  const from = await User.updateOne(
    { _id: req.user._id },
    { $push: { pendingRequests: pendingRequestFrom } },
    errorHandler()
  );

  // assign request to user which should accept
  const pendingRequestTo = {
    friendId: req.user._id,
    isFrom: true,
    name: req.user.name,
  };

  let to = await User.updateOne(
    { _id: friendId },
    { $push: { pendingRequests: pendingRequestTo } },
    errorHandler()
  );

  res.sendStatus(201);
});

//api/friend/acceptFriend/    -   Accept Friend request
router.put("/acceptFriend/:friendId", async (req, res) => {
  // add a friend to the friend list of the person who accepted the request
  const friendId = req.params["friendId"];
  let friendFrom = {
    friendId,
    name: req.body.name,
  };

  const from = await User.findByIdAndUpdate(
    { _id: req.user._id },
    {
      $push: { friends: friendFrom },
      $pull: { pendingRequests: { friendId } },
    },
    errorHandler()
  );

  // add a friend to the friend list of the person who send the request

  let friendTo = {
    friendId: req.user._id,
    name: req.user.name,
  };

  const to = await User.findByIdAndUpdate(
    { _id: friendId },
    {
      $push: { friends: friendTo },
      $pull: { pendingRequests: { friendId: req.user._id } },
    },
    errorHandler()
  );

  res.sendStatus(201);
});

//api/friend/cancelFriend/    -   Cancel Friend request  && Ignore Friend request
router.delete("/cancelFriend/:friendId", async (req, res) => {
  // delete the request from the user who deletes
  const friendId = req.params["friendId"];
  let friendFrom = {
    friendId,
  };

  const from = await User.updateOne(
    { _id: req.user._id },
    {
      $pull: { pendingRequests: { friendId } },
    },
    errorHandler()
  );

  // delete the request from the user who has send the request

  let friendTo = {
    friendId: req.user._id,
  };

  const to = await User.updateOne(
    { _id: friendId },
    {
      $pull: { pendingRequests: { friendId: req.user._id } },
    },
    errorHandler()
  );

  res.sendStatus(204);
});

//api/friend/deleteFriend/    -   Remove Friend
router.delete("/deleteFriend/:friendId", async (req, res) => {
  // delete friend from friend list of the person who accepted the request
  const friendId = req.params["friendId"];
  let friendFrom = {
    friendId,
  };

  const from = await User.updateOne(
    { _id: req.user._id },
    {
      $pull: { friends: friendFrom },
    },
    errorHandler()
  );

  // delete friend from the friend list of another person

  let friendTo = {
    friendId: req.user._id,
  };

  const to = await User.updateOne(
    { _id: friendId },
    {
      $pull: { friends: friendTo },
    },
    errorHandler()
  );

  res.sendStatus(204);
});

function errorHandler(err, result) {
  if (err) {
    console.log(err);
    res.send(err);
    return;
  }
}

module.exports = router;
