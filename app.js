const express = require('express');
const config = require('config');
const mongoose = require('mongoose');

const app = express();
app.use('/api/auth',require('./routes/auth.routes'));
 app.use('/api/friend',require('./routes/friend.routes'));
 app.use('/api/user',require('./routes/user.routes'));

app.use('/',require('./routes/routing'));




const PORT = config.get('port') || 80;


async function start() {
    try {
        await mongoose.connect(config.get('mongoUri'), {
            useNewUrlParser : true ,
            useUnifiedTopology: true,
            useCreateIndex: true
        });
        app.listen(PORT, () => console.log(`server was started on port ${PORT} ...`));

    } catch (e) {
        console.log('error', e.message); 
        proces.exit(1);
    }




}

start();
