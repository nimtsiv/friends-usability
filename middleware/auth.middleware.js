const User = require('../models/User');
const bcrypt = require('bcryptjs');

module.exports = {
    ensureToken: async function (req, res, next) {
        
        const authorization = req.headers["authorization"];
        if (typeof authorization !== 'undefined') {
            const authData = authorization.split(" ");
            let buff = new Buffer.from( authData[1], 'base64');
            let text = buff.toString('ascii');
            console.log(text);

            const loginDataArray = text.split(':');
            console.log(loginDataArray)

            let user = await User.findOne({email : loginDataArray[0]});
            console.log(user);
            
            
            if (user !== null) {
                const match = await bcrypt.compare(loginDataArray[1], user.password);
            

                if (!match) {
                    res.message ='Для роботи пройдіть реєстрацію';
                    res.sendStatus(403);
                }
    
              
                req.user = user;
                next();
            }else{
                res.message ='Для роботи пройдіть реєстрацію';
                    res.sendStatus(403);
            }

        } else {
            res.message ='Для роботи пройдіть реєстрацію';
            res.sendStatus(403);
        }

    },
    withoutAuth: function (req, res, next){
        res.header("Access-Control-Allow-Origin", '*');
        res.header("Access-Control-Allow-Headers", 'Origin, X-Request-With, Content-Type, Accept, Authorization');
        res.header("Access-Control-Expose-Headers", 'Authorization, Uid');
        next();
    },
    withAuth: function (req, res, next){
        res.header("Access-Control-Allow-Origin", "http://localhost:3000");
        res.header("Access-Control-Allow-Headers", "Origin, X-Request-With, Content-Type, Accept, Authorization");
        res.header("Access-Control-Allow-Credentials", 'true');
        
    
        
        if(req.method === 'OPTIONS'){
          res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')
          return res.status(200).json({});
        }
        next();
      }
 
};
