const {Schema, model} = require ('mongoose');

const schema = new Schema({
    email:{ type : String, required: true, unique:true},
    password:{ type : String, required: true},
    name:{ type : String, required: true},
    friends:[{ 
      friendId:{type : Schema.Types.ObjectId, ref: "User" },
      name:String
    }],
    pendingRequests:[{
        friendId:{type : Schema.Types.ObjectId, unique:true , ref: "User" },
        isFrom:Boolean,
        name:String,
    }],
  });

module.exports = model ('User', schema);