import React, { Component } from "react";
import axios from "axios";
import './login.css'

export default class Login extends Component {
  constructor(props) {
    super(props);

    if (localStorage.getItem('authorization')!== null){
      this.props.history.push("/");
    };

    this.state = {
      email: "",
      password: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(event) {
     let thisone = this;
    axios
      .post("http://localhost/api/auth/login", {
        email: this.state.email,
        password: btoa(this.state.password),
      })
      .then(function (response ) {
        if ((response.status === 200)) {  
          localStorage.setItem( 'authorization' , response.headers.authorization );
          thisone.props.history.push("/");
        }
      })
       .catch(err => {
         console.log(err)
       alert(err.response.data.message);
    });
    event.preventDefault();
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input
            type="email"
            name="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleChange}
            required
          />

          <input
            type="password"
            name="password"
            placeholder="Password"
            value={this.state.password}
            onChange={this.handleChange}
            required
          />
          <button type="submit"> LogIn </button>

          <h3> Existed users </h3>
          <p> testUser@mail.com   </p>
          <p> testUser1@mail.com  </p>
          <p> testUser2@mail.com  </p>
          <p> testUser3@mail.com  </p>
          <p> testUser4@mail.com  </p>
          <h3> passwrd  </h3>
          <p> 123123  </p>
        </form>
      </div>
    );
  }
}
