import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "../Home/Home";
import Login from "../auth/Login/Login";

export default class App extends Component {

  render() {
    return (
      <div className="app">
        <BrowserRouter>
          <Switch>
            <Route exact path={"/"} component={Home} />

            <Route exact path={"/login"} component={Login} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}
