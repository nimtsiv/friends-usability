import React, { Component } from "react";
import axios from "axios";
import User from '../User/User'

export default class FriendsTab extends Component {
  constructor(props) {
    super(props);

    const auth = props.parrentState.auth;

    this.state = {
      auth,
      incomeRequest: [],
      OutgoinRequest: [],
      friends: [],
    };
  }
   componentDidMount() {
     this.refreshUserData();
   
  }

  refreshUserData() {
    const config = {
      method: "get",
      url: "http://localhost/api/user/",
      headers: {
        Authorization: this.state.auth,
      },
      data: "",
    };
    axios(config)
      .then((response) => {
        if(response.status === 200)
        this.prepareData (response.data )
      })
      .catch((err) => {
        console.log(err);
        // alert(err.response.data.message);
      });
  }
  prepareData(data) {
    this.setIncomeRequest(data);
    this.setOutgoinRequest(data);
    this.setState({
      friends: data.friends
    });

  }
  setOutgoinRequest(data) {

    const OutgoinRequest = data.pendingRequests.filter(
      (record) => record.isFrom === false
    );
   
    this.setState({
      OutgoinRequest
    });
  }

  setIncomeRequest(data) {

    const incomeRequest = data.pendingRequests.filter(
      (record) => record.isFrom === true
    );
   
    this.setState({
        incomeRequest
    });
  }

  componentDidUpdate() {}
  render() {
    console.log(this.state);
    return (
      <div className="content">
        <h1>Pending request</h1>
        <h2 className="alignleft">Incoming</h2>
        {this.state.incomeRequest.map((user, index) => {
          return (
            <User
              name={user.name}
              userId={user._id}
              action={'AccIgnoreFriend'}
              auth={this.state.auth}
              refresh={this.refreshUserData}
            />
          );
        })}

        <h2 className="alignleft">Outcoming</h2>
        {this.state.OutgoinRequest.map((user, index) => {
          return (
            <User
              name={user.name}
              userId={user._id}
              action={'CancleFriendrequest'}
              auth={this.state.auth}
              refresh={this.refreshUserData}
            />
          );
        })}
        <h1>Friends</h1>
        {this.state.friends.map((user, index) => {
          return (
            <User
              name={user.name}
              userId={user._id}
              action={'RemoveFriend'}
              auth={this.state.auth}
              refresh={this.refreshUserData}
            />
          );
        })}
      </div>
    );
  }
}
