import React, { Component } from "react";
import axios from "axios";
import User from "../User/User";

export default class UsersTab extends Component {
  constructor(props) {
    super(props);

    const auth = props.parrentState.auth;

    this.state = {
      auth,
      searchResult: [],

    };

    this.handleChange = this.handleChange.bind(this);
    this.refresh = this.refresh.bind(this);
  }

   componentDidMount() {
  this.refresh();
  }
  
 
refresh() {
  const config = {
    method: "get",
    url: "http://localhost/api/user/",
    headers: {
      Authorization: this.state.auth,
    },
    data: "",
  };
  axios(config)
    .then((response) => {
      this.setState({ userData: response.data });
    })
    .catch((err) => {
      console.log(err);
      // alert(err.response.data.message);
    });

}

  handleChange(event) {
    const val = event.target.value;
 

    if (val.length > 3) {
      let config = {
        method: "get",
        url: "http://localhost/api/friend/search/" + val,
        headers: {
          Authorization: this.state.auth,
        },
        data: "",
      };
      axios(config)
        .then( response=> {
          if (response.status === 200) {
            const userData = this.state.userData;
            const searchResult = response.data.map(function (user, index) {
              let action;
              const isFriend = userData.friends.filter(
                (friend) => friend.friendId === user._id
              );

              if (isFriend.length !== 0) {
                action = "RemoveFriend";
              }

              const havePendingReq = userData.pendingRequests.filter(
                (request) => request.friendId === user._id
              );
              if (havePendingReq.length !== 0) {
                if (havePendingReq[0].isFrom) {
                  action = "AccIgnoreFriend";
                } else {
                  action = "CancleFriendrequest";
                }
              }

              if (havePendingReq.length === isFriend.length) {
                action = "AddFriend";
              }
              return {
                name: user.name,
                _id: user._id,
                action,
              };
            });

            this.setState({
              searchResult,
            });
          } else {
            this.setState({
              searchResult: [],
            });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      this.setState({
        searchResult: [],
      });
    }
  }
  render() {
    // console.log(this.state.userData);
    return (
      <div className="content">
        <h1>UserTab</h1>
        <input type="text" name="search" onChange={this.handleChange} />
        {this.state.searchResult.map((user, index) => {
          return (
            <User
              name={user.name}
              userId={user._id}
              action={user.action}
              auth={this.state.auth}
              refresh={this.refresh}
            />
          );
        })}
      </div>
    );
  }
}
