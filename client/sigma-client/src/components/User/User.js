import React, { Component } from "react";
import UserInfo from "./UserInfo";
import RemoveFriend from "./Actions/RemoveFriend";
import CancleFriendrequest from "./Actions/CancleFriendrequest";
import AddFriend from "./Actions/AddFriend";
import AccIgnoreFriend from "./Actions/AccIgnoreFriend";

export default class User extends Component {
  components = {
    RemoveFriend: RemoveFriend,
    CancleFriendrequest: CancleFriendrequest,
    AddFriend: AddFriend,
    AccIgnoreFriend: AccIgnoreFriend,
  };

  render(props) {
    const ActionTagName = this.components[this.props.action];
    return (
      <div>
        <UserInfo userName={this.props.name}>
         

          <ActionTagName  name={this.props.name} userId={this.props.userId} auth={this.props.auth} refresh={this.props.refresh }/>
         
        </UserInfo>
      </div>
    );
  }
}
