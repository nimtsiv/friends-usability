import React from "react";
import './UserInfo.css'

export default function UserInfo(props) {
  return (
    <div className="UserInfo">
      <div>
        <img src="user-png-icon-male-user-icon-512.png" alt="user icon" />
        <h3>{props.userName}</h3>
      </div>
      {props.children}
    </div>
  );
}
