import React, { Component } from "react";
import axios from "axios";

export default class RemoveFriend extends Component {
  constructor(props) {
    super(props);

    const auth = props.auth;
    const userId = props.userId;
    this.refresh = props.refresh;

    this.state = {
      auth,
      userId,
      deleted: false,
    };

    this.RemoveFriendhandler = this.RemoveFriendhandler.bind(this);
  }

  RemoveFriendhandler() {
    let config = {
      method: "delete",
      url: "http://localhost/api/friend/deleteFriend/" + this.state.userId,
      headers: {
        Authorization: this.state.auth,
      },
      data: "",
    };
    axios(config)
      .then((response) => {
        if (response.status === 204)
          this.setState({
            deleted: true,
          });
      })
      .catch((err) => {
        console.log(err);
      });
      this.refresh();
  }
  render(props) {
    if (this.state.deleted) {
      return (
        <div className="actions">
          <h3>friends was deleted</h3>
        </div>
      );
    } else {
      return (
        <div className="actions">
          <h3>friends</h3>
          <button onClick={this.RemoveFriendhandler}>RemoveFriend</button>
        </div>
      );
    }
  }
}
