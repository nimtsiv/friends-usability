import React, { Component } from 'react'
import axios from 'axios'

export default class AccIgnoreFriend extends Component {
    constructor(props) {
        super(props);
    
        const auth = props.auth;
        const userId = props.userId;
        const name = props.name;
        this.refresh = props.refresh;
    
        this.state = {
          name,
          auth,
          userId,
          sended: false,
        };
    
        this.AcceptFriendhandler = this.AcceptFriendhandler.bind(this);
        this.IgnoreFriendhandler = this.IgnoreFriendhandler.bind(this);
      }
    
      IgnoreFriendhandler() {
        let config = {
          method: "delete",
          url: "http://localhost/api/friend/cancelFriend/" + this.state.userId,
          headers: {
            Authorization: this.state.auth,
          },
          data: JSON.stringify({name : this.state.name}),
        };
        axios(config)
          .then((response) => {
            if (response.status === 204)
              this.setState({
                sended: true,
              });
          })
          .catch((err) => {
            console.log(err);
          });
          this.refresh();
      }

      AcceptFriendhandler() {
        
        fetch('http://localhost/api/friend/acceptFriend/'+this.state.userId, {
          method: 'PUT',headers: {
              Authorization: this.state.auth,
              'Content-type': 'application/json'
            },
          body: JSON.stringify({name:this.state.name})
          })
          .then((response) => {
            if (response.status === 201)
              this.setState({
                sended: true,
              });
          })
          .catch((err) => {
            console.log(err);
          });
          this.refresh();
      }
      render(props) {
        if (this.state.sended) {
          return (
            <div className="actions">
              <h3>Request was sended</h3>
            </div>
          );
        } else {
          return (
            <div className="actions">
              <h3> pending request </h3>
              <button onClick={this.AcceptFriendhandler}>Accept</button>
              <button onClick={this.IgnoreFriendhandler}>Ignore</button>
            </div>
          );
        }
      }
    }
    