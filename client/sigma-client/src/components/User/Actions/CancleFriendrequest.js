import React, { Component } from 'react'
import axios from 'axios'

export default class CancleFriendrequest extends Component {
    constructor(props) {
        super(props);
    
        const auth = props.auth;
        const userId = props.userId;
        this.refresh = props.refresh;
    
        this.state = {
          auth,
          userId,
          cancled:false
        };
    
        this.CancleFriendrequest = this.CancleFriendrequest.bind(this);
      }
      
      CancleFriendrequest (){

        let config = {
            method: "delete",
            url: "http://localhost/api/friend/cancelFriend/" + this.state.userId,
            headers: {
              Authorization: this.state.auth,
            },
            data: "",
          };
          axios(config)
            .then(response=> {
                    if(response.status=== 204)
                    this.setState({
                        cancled: true
                    })
                })
            .catch((err) => {
              console.log(err);
            });
            this.refresh();
      }

    render(props) {
        if(this.state.cancled){
            return (
                <div className="actions">
                    <h3>request was cancled</h3>
                </div>
            )}
        else
        {       
             return (
            <div className="actions">
                <h3>pending request</h3>
                <button onClick={this.CancleFriendrequest}>Cancle request</button>
            </div>
        )
    }
    }
}
