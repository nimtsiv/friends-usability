import React, { Component } from 'react'

export default class AddFriend extends Component {
    constructor(props) {
        super(props);
    
        const auth = props.auth;
        const userId = props.userId;
        const name = props.name;
        this.refresh = props.refresh;
    
        this.state = {
          auth,
          name,
          userId,
          added: false,
        };
    
        this.AddFriendhandler = this.AddFriendhandler.bind(this);
      }
    
      AddFriendhandler() {
       
        fetch('http://localhost/api/friend/addFriend/'+this.state.userId, {
        method: 'PUT',headers: {
            Authorization: this.state.auth,
            'Content-type': 'application/json'
          },
        body: JSON.stringify({name:this.state.name})
         })
          .then((response) => {
            if (response.status === 201)
              this.setState({
                added: true,
              });
          })
          .catch((err) => {
            console.log(err);
          });
          this.refresh();
      }
      render(props) {
        if (this.state.added) {
          return (
            <div className="actions">
              <h3>Request wass ended</h3>
            </div>
          );
        } else {
          return (
            <div className="actions">
              <h3>friends</h3>
              <button onClick={this.AddFriendhandler}>Add Friend</button>
            </div>
          );
        }
      }
    }
    