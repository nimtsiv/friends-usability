import React, { Component } from "react";
import FriendsTab from "../FriendsTab/FriendsTab";
import UsersTab from "../UsersTab/UsersTab";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import "./home.css";

export default class Home extends Component {
  constructor(props) {
    super(props);
    const auth = localStorage.getItem("authorization");

    if (auth === null) {
      this.props.history.push("/login");
    }

    this.state = {
      auth,
    };

    this.logOutHandler = this.logOutHandler.bind(this);
  }

  logOutHandler() {
    localStorage.removeItem("authorization");
    this.props.history.push("/login");
  }

  render() {
    return (
      <div>
        <button onClick={this.logOutHandler} className="logoutButton"> log Out</button>
        <Tabs>
          <TabList>
            <Tab>User</Tab>
            <Tab>Friend</Tab>
          </TabList>

          <TabPanel>
            <UsersTab parrentState={this.state} />
          </TabPanel>
          <TabPanel>
            <FriendsTab parrentState={this.state} />
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}
